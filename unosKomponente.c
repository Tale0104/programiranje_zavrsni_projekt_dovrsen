#include "Header.h"

void unosKomponente() {

	int ukupanBroj = 0;
	int brKomponente;

	KOMPONENTE* komp = NULL;
	FILE* fpbr1 = NULL;

	fpbr1 = fopen("ukupanBroj.txt", "r");
	if (fpbr1 == NULL) {
		printf("\nDatoteka je stvorena.\n");
		FILE* fpbr = fopen("ukupanBroj.txt", "w+");
		int k = 0;
		fprintf(fpbr, "%d", k);
		fclose(fpbr);
	}
	else {
		fscanf(fpbr1, "%d", &ukupanBroj);
		fclose(fpbr1);
	}
	printf("\nUnesite broj komponenti koje zelite unijeti: ");
	scanf("%d", &brKomponente);

	komp = (KOMPONENTE*)malloc(brKomponente * sizeof(KOMPONENTE));

	FILE* fp = NULL;
	fp = fopen("unosKomponente.bin", "a+b");
	if (fp == NULL) {
		fprintf(stderr, "Vrijednost pogreske je: %d.\n", errno);
		fprintf(stderr, "Pogreska: %s.\n", strerror(errno));
		return 0;
	}
	else {
		printf("\nUnesite podatke o komponenti.\n");
		for (int i = 0; i < brKomponente; i++) {
			printf("\nUnesite ime %d. komponente: ", i+1);
			scanf("%30s", komp[i].ime);
			printf("\nUnesite proizvodaca %d. komponente: ", i+1);
			scanf("%30s", komp[i].proizvodac);
			printf("\nUnesite oznaku %d. komponente: ", i+1);
			scanf("%30s", komp[i].oznaka);
			printf("\nUnesite model %d. komponente: ", i+1);
			scanf("%d", &komp[i].model);
			printf("\nUnesite cijenu %d. komponente: ", i+1);
			scanf("%f", &komp[i].cijena);
			printf("\nUnesite stanje %d. komponente: ", i+1);
			scanf("%d", &komp[i].stanje);
			fwrite(&komp[i], sizeof(KOMPONENTE), 1, fp);
			ukupanBroj++;
		}
		fclose(fp);
		free(komp);
		fpbr1 = fopen("ukupanBroj.txt", "w");
		fprintf(fpbr1, "%d", ukupanBroj);
		fclose(fpbr1);
		printf("\n 1.Povratak na glavni izbornik.\n 2. Izlaz iz aplikacije\n");
		int povratak;
		printf("\n Odaberite opciju koju zelite koristiti: ");
		scanf("%d", &povratak);
		if (povratak == 1) {

		}
		else if (povratak == 2) {
			izlaz();
		}
	}

}