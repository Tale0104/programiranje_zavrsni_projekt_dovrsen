#include "Header.h"

void pregledKomponente() {

	int br;
	FILE* fpbr1 = NULL;
	fpbr1 = fopen("ukupanBroj.txt", "r");
	fscanf(fpbr1, "%d", &br);
	fclose(fpbr1);
	KOMPONENTE* komp = NULL;
	FILE* fp = NULL;
	fp = fopen("unosKomponente.bin", "a+b");
	if (fp == NULL) {
		fprintf(stderr, "Vrijednost pogreske je: %d.\n", errno);
		fprintf(stderr, "Pogreska: %s.\n", strerror(errno));
		return 0;
	}
	else {
		printf("\n------POPIS KOMPONENTI------\n");
		printf("\nBroj komponenti u popisu: %d\n", br);

		komp = (KOMPONENTE*)malloc(sizeof(KOMPONENTE));
		for (int i = 0; i < br; i++) {
			fread(komp, sizeof(KOMPONENTE), 1, fp);
			printf("\nIme: %s\nProizvodac: %s\nOznaka: %s\nModel: %d\nCijena: %.2f HRK\nStanje: %d\n", komp->ime, komp->proizvodac, komp->oznaka, komp->model, komp->cijena, komp->stanje);
		}
		fclose(fp);
		free(komp);


		printf("\n1. Povratak u glavni izbornik\n2. Izlaz iz aplikacije\n");
		int povratak;
		printf("\nOdaberite opciju koju zelite koristiti: ");
		do {
			scanf("%d", &povratak);
		} while (povratak < 1 || povratak>2);
		if (povratak == 1) {
		}
		else if (povratak == 2) {
			izlaz();
		}
	}
}