#include "Header.h"

int main() {

	int brKomp = 0;
	int n;
	do {
		printf("\n******SKLADISTE RACUNALNOM OPREMOM******\n");
		printf("\tDobrodosli !\n");
		printf("\nUnesite broj opcije koju zelite koristiti.\n\n");
		printf("1. \tUnos komponente\n");
		printf("2. \tPregled komponente\n");
		printf("3. \tPretraga komponenti\n");
		printf("4. \tBrisanje komponenti\n");
		printf("5. \tIzlaz\n");
		printf("\nOdabir opcija: ");
		do {
			scanf("%d", &n);
			if (n < 1 || n>5) {
				printf("Unos je neispravan, molim vas pokusajte ponovo.");
			}
		} while (n < 1 || n>5);
		if (n == 1) {
			unosKomponente();
		}
		else if (n == 2) {
			pregledKomponente();
		}
		else if (n == 3) {
			pretragaKomponente();
		}
		else if (n == 4) {
			brisanjeKomponente();
		}
		else if (n == 5) {
			izlaz();
		}
	} while (1);
}