#ifndef HEADER_H
#define HEADER_H

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct komponente{
	char ime[31];
	char proizvodac[31];
	char oznaka[31];
	int model;
	float cijena;
	int stanje;
}KOMPONENTE;

void unosKomponente();
void pregledKomponente();
void pretragaKomponente();
void brisanjeKomponente();
int izlaz();

#endif//HEADER_H